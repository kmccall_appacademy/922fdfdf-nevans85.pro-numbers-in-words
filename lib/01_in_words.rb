class Fixnum
  def in_words
    return 'zero' if zero?
    result = []
    (0..count_triplets).each do |idx|
      unless get_triplet(idx).zero?
        result << TRIPLET_NAMES[idx] unless idx.zero?
        result << triplet_to_s(get_triplet(idx))
      end
    end
    result.reverse.join(' ')
  end

  private

  TRIPLET_NAMES = ['', 'thousand', 'million', 'billion', 'trillion'].freeze
  DIGIT_NAMES = { 1 => 'one', 2 => 'two', 3 => 'three',
                  4 => 'four', 5 => 'five', 6 => 'six',
                  7 => 'seven', 8 => 'eight', 9 => 'nine',
                  10 => 'ten', 11 => 'eleven', 12 => 'twelve',
                  13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
                  16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
                  19 => 'nineteen' }.freeze
  TENS_NAMES = { 2 => 'twenty', 3 => 'thirty',
                 4 => 'forty', 5 => 'fifty', 6 => 'sixty',
                 7 => 'seventy', 8 => 'eighty', 9 => 'ninety' }.freeze

  def triplet_to_s(num)
    result = []
    if num / 100 > 0
      result << DIGIT_NAMES[num / 100] << 'hundred'
      num -= (num / 100) * 100
    end
    if num / 10 > 1
      result << TENS_NAMES[num / 10]
      num -= (num / 10) * 10
    end
    result << DIGIT_NAMES[num] if num > 0
    result.join(' ')
  end

# get_triplet method returns a triplet of the Fixnum. Idx as follows:
#                9,876,543,210
#           idx: 3  2   1   0
  def get_triplet(idx)
    (self % (1000**(idx + 1))) / 1000**idx
  end

  def count_triplets
    count = 0
    count += 1 while self / 1000**count > 0
    count
  end
end
